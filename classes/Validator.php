<?php 

/**
 * 
 */
class Validator 
{
	public static function rePassword($password, $rePassword)
	{
		if($password !== $rePassword){
	        $response = [
	            'code' => 400,
	            'message' => 'Validation error: passwords do not match!',
	            'status' => false
	        ];
	        echo json_encode($response);
	        exit;
    	}else{
    		return true;
    	}
	}	

	public static function validateInput($type, $input) {
	  switch ($type) {
	    case "email":
	      $regex = '/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,})+$/';
	      break;
	    case "phone":
	      $regex = '/^\+374\d{8}$/';
	      break;
	    case "password":
	      $regex = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/';
	      break;
	    default:
	      return false;
	  }
	  return preg_match($regex, $input);
	}

}
 ?>