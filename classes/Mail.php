<?php 

/**
 * 
 */
require_once('models/User.php');
require_once('Output.php');

class Mail 
{
	public static function sendActivationEmail($email) {
	  // Generate a random activation code
	  $activationCode = substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 10)), 0, 10);

	  // Compose the email message
	  $to = $email;
	  $subject = 'Account activation';
	  $message = "Hello, $email, \n\nYour activation code is: $activationCode\n\nPlease use this code to activate your account.";

	  // Send the email
	  $headers = "From: armen-authsystem.com\r\n";
	  $headers .= "Reply-To: armen-authsystem.com\r\n";
	  $headers .= "Content-type: text/plain; charset=UTF-8\r\n";
	  $headers .= "MIME-Version: 1.0\r\n";
	  //$success = mail($to, $subject, $message, $headers);
	  $success = true;
	  self::save_activation_email($message);
	  if ($success && User::setActivationCode($email, $activationCode)) {
	  		  return $success;
	  }else{
	  	 echo Output::activationCodeSetError();
	  }
	}

	public static function save_activation_email($activation_email) {
	    // create the mail folder if it doesn't already exist
	   try {
		   	 if (!file_exists("mails")) {
		        mkdir("mails");
		    }
		    
		    // generate a unique file name for the activation email
		    $file_name = "activation_email_" . time() . ".txt";
		    
		    // create the activation email file
		    $file = fopen("mails/" . $file_name, "w");
		    fwrite($file, $activation_email);
		    fclose($file);
	   } catch (Exception $e) {
	   	 echo json_encode($e->getMessage());
	   }
	}


}
 ?>