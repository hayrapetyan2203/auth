function validate(type, data){
	switch(type) {
	  case 'email':
	    return validateEmail(data);
	  case 'phonenumber':
	    return validatePhonenumber(data);
	  case 'password':
	    return validatePassword(data);
	  default:
	    return false;
	}
}

function validateEmail(email) {
  const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,})+$/
  return emailRegex.test(email)
}

function validatePhonenumber(phonenumber) {
  const phoneRegex = /^\+374\d{8}$/;
  return phoneRegex.test(phonenumber);
}


function validatePassword(password) {
  const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
  return passwordRegex.test(password);
}

function setSuccess(){
console.log('sdsd');
	$("#rePassword").on('input', function() {
	  if ($("#rePassword").val() === $("#password").val()) {
	    $("#rePasswordError").removeClass('text-danger')
	    $("#rePasswordError").addClass('text-success')
	  }
	})
	
	// Define an array of objects that store information about each input element
	const inputs = [
	  { id: 'email', validate: 'email' },
	  { id: 'phonenumber', validate: 'phonenumber' },
	  { id: 'password', validate: 'password' }
	];

	// Bind the input event handler to each input element
	inputs.forEach(input => {
	  $('#' + input.id).on('input', function() {
	    if (validate(input.validate, $('#' + input.id).val())) {
	      $('#' + input.id + 'Error').removeClass('text-danger');
	      $('#' + input.id + 'Error').addClass('text-success');
	    }
	  });
	});
}