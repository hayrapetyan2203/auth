function login(email, password){
    $.ajax({
        type: 'POST',
        url: 'classes/index.php',
        data: {email : email,  password : password,  login : true},
        beforeSend: function() {
            
        },
        success: function(data) {
            let response = JSON.parse(data)
            if(response.code == 200){
                window.location.href = "./index.php?action=profile";
            }

            if (response.code == 400) {
                $('emailError').addClass('text-danger')
                $('passwordError').addClass('text-danger')
            }
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
           
        },
    });
}
