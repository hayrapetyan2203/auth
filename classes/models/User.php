<?php 
require_once('Connect.php');
/**
 * 
 */
class User 
{
	public static function setSessionInfo($email)
	{
		try {
			$_SESSION['email'] = $email;
			$_SESSION['SESSION_ID'] = $_COOKIE['PHPSESSID'];
			$user_id = self::getUserId($email);
			$_SESSION['user_id'] = $user_id;
			$session_id = $_COOKIE['PHPSESSID'];
			self::setSessionTable($user_id, $email, $session_id);
		} catch (Exception $e) {
			print_r($e->getMessage());
			return false;
		}
		return true;
	}

	public static function setSessionTable($user_id, $email, $session_id)
	{
	   $dbConn = Connect::getConnection();
	   $stmt = $dbConn->prepare("INSERT INTO usersession (user_id, email, session_id) VALUES (:user_id, :email, :session_id)");
	   $stmt->bindParam(':user_id', $user_id);
	   $stmt->bindParam(':email', $email);
	   $stmt->bindParam(':session_id', $session_id);
	   $success = $stmt->execute();

	   return $success;
	}

	public static function getUserId($email)
	{
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("SELECT id FROM users WHERE email = :email ");
	    $UserExistsQuery->bindParam(':email', $email);
	    $UserExistsQuery->execute();
	    $user = $UserExistsQuery->fetch(PDO::FETCH_ASSOC);
	    return ($user['id'] !== false);
	}

	public static function exists($email, $phonenumber) {
	  $dbConn = Connect::getConnection();
	  $UserExistsQuery = $dbConn->prepare("SELECT email, phonenumber FROM users WHERE email = :email AND phonenumber = :phonenumber");
	  $UserExistsQuery->bindParam(':email', $email);
	  $UserExistsQuery->bindParam(':phonenumber', $phonenumber);
	  $UserExistsQuery->execute();
	  $user = $UserExistsQuery->fetch(PDO::FETCH_ASSOC);
	  return ($user !== false);
	}

	public static function CanLogin($email, $password) {
	  $dbConn = Connect::getConnection();
	  $UserExistsQuery = $dbConn->prepare("SELECT email, password FROM users WHERE email = :email");
	  $UserExistsQuery->bindParam(':email', $email);
	  $UserExistsQuery->execute();
	  $user = $UserExistsQuery->fetch(PDO::FETCH_ASSOC);
	  return (password_verify($password, $user['password']));
	}

	public static function setActivationCode($email, $activationCode) {
	  $dbConn = Connect::getConnection();

	  // Set the expiration time to 3 minutes from now
	  $expireTime = date('Y-m-d H:i:s', strtotime('+3 minutes'));

	  // Insert the activation code into the userActivation table
	  $stmt = $dbConn->prepare("INSERT INTO useractivation (email, activationCode, activationCodeExpire) VALUES (:email, :activationCode, :expireTime)");
	  $stmt->bindParam(':email', $email);
	  $stmt->bindParam(':activationCode', $activationCode);
	  $stmt->bindParam(':expireTime', $expireTime);
	  $success = $stmt->execute();

	  return $success;
	}

	public static function getActivationCode()
	{
		$currentDate = date("Y-m-d H:i:s");
		$activationCode = $_POST['activationCode'] ?? false;
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("SELECT * FROM useractivation WHERE activationCode = :activationCode AND TIME_TO_SEC(TIMEDIFF(activationCodeExpire, :currentDate)) > -180");
	    $UserExistsQuery->bindParam(':activationCode', $activationCode);
	    $UserExistsQuery->bindParam(':currentDate', $currentDate);
	    $UserExistsQuery->execute();
	    $user = $UserExistsQuery->fetch(PDO::FETCH_ASSOC);
	    if($user){
	    	self::Activate($activationCode, $user['email']);
	    }else{return false;}
	}

	public static function Activate($activationCode, $email)
	{
		//activate the user
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("UPDATE users SET is_activated = 1 WHERE email = :email");
	    $UserExistsQuery->bindParam(':email', $email);
	  	if($UserExistsQuery->execute()){
	  		header('Location: ' . './index.php?action=login');
	  	}
	}


}
 ?>