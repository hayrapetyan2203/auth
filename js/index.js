$(document).ready(function(){

	$("input").on('input', function() {
	  	setSuccess()
	})

	$("#register").click(function(){
		var email = $("#email").val()
		var phonenumber = $("#phonenumber").val()
		var password = $("#password").val()
		var rePassword = $("#rePassword").val()

		if (validate('email', email) && validate('phonenumber', phonenumber) && validate('password', password) && password === rePassword) {
			register(email, phonenumber, password, rePassword)
		}else{
			showErrors(email, phonenumber, password)
		}
	})

	$("#login").click(function(){
		let email = $("#email").val()
		let password = $("#password").val()
		var phonenumber = $("#phonenumber").val()

		if (validate('email', email) &&  validate('password', password)) {
			login(email, password)
		}else{
			showErrors(email, phonenumber, password)
		}

	})
})