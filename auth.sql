-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 04 2023 г., 18:49
-- Версия сервера: 10.4.19-MariaDB
-- Версия PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `auth`
--

-- --------------------------------------------------------

--
-- Структура таблицы `useractivation`
--

CREATE TABLE `useractivation` (
  `id` int(12) NOT NULL,
  `email` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activationCode` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activationCodeExpire` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `useractivation`
--

INSERT INTO `useractivation` (`id`, `email`, `activationCode`, `activationCodeExpire`) VALUES
(1, 'hayrapetyan2203@gmail.com', 'srNH5FqCXh', '2023-04-02 13:01:46');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `email` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `phonenumber`, `password`, `is_activated`) VALUES
(4, 'hayrapetyan2203@gmail.com', '+37477611707', 'Qajari98*', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `usersession`
--

CREATE TABLE `usersession` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `session_id` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `useractivation`
--
ALTER TABLE `useractivation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `usersession`
--
ALTER TABLE `usersession`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `useractivation`
--
ALTER TABLE `useractivation`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `usersession`
--
ALTER TABLE `usersession`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
