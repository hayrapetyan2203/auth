<?php 
session_start(); 
require_once('classes/Sessions.php');
Session::Check();

$sessionCount = Session::getUserSessionsCount($_SESSION['user_id']);
$sessions = Session::getUserSessions($_SESSION['user_id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Profile</title>
	<link rel="stylesheet" href="css/profile.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<div class="p-5" align="center">
    <div class="col-md-8">
        <!-- Column -->
        <div class="card"> <img class="card-img-top" src="https://i.imgur.com/K7A78We.jpg" alt="Card image cap">

<div align="right" class="p-2 m-2" id="lougoutButton" style="cursor: pointer;">
    <input type="hidden" value="<?=$_COOKIE['PHPSESSID']; ?>">
	<svg  xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="red" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
	  <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
	  <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
	</svg>
</div>
            <div class="card-body little-profile text-center">
                <div class="pro-img"><img src="https://i.imgur.com/8RKXAIV.jpg" alt="user"></div>
                <h3 class="m-b-0">Brad Macullam</h3>
                <p>Web Designer &amp; Developer</p> 
                <div class="row text-center m-t-20">
                    <div class="col-lg-4 col-md-4 m-t-20">
                        <h3 class="m-b-0 font-light">10434</h3><small>Articles</small>
                    </div>
                    <div class="col-lg-4 col-md-4 m-t-20">
                        <h3 class="m-b-0 font-light">434K</h3><small>Followers</small>
                    </div>
                   <div class="col-lg-4 col-md-4 m-t-20">
					  <div class="dropdown">
					    <h3 data-toggle="dropdown" class="m-b-0 font-light"><?=$sessionCount; ?></h3><small>Active Sessions</small>
					    <div class="dropdown-menu" style="position: absolute; top: 100%; left: 0; display: none;">
					      <?php foreach ($sessions as $session) { ?>
					      	<div class="d-flex">
					      		<form action="" id="deleteSessionsForm" class="p-1">
					      			<input type="hidden" value="<?php echo $session['id']; ?>">
					      			<a class="dropdown-item" 	 href="#"><?php echo $session['session_id']; ?></a>
					      		</form>
					      	</div>
					     <?php } ?>
					    </div>
					  </div>
					</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
  $(function() {
    $('.dropdown h3').click(function(e) {
      e.preventDefault();
      $('.dropdown-menu').toggle();
    });
  });  


$('a.dropdown-item, #lougoutButton').click(function(e) {
  e.preventDefault();
  var session = 0;
   if ($(e.target).hasClass('dropdown-item')) {
     session = $(this).text();
  }else{
     session = $(e.target).find('input').val();
  }
  var deleteSession = true;
    $.ajax({
        type: 'POST',
        url: 'classes/index.php',
        data: {session : session, deleteSession : deleteSession},
        beforeSend: function() {
            
        },
        success: function(data) {
            location.replace(location.href)
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
           
        },
    });

});

</script>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>