<?php 
session_start();
/**
 * 
 */
require_once('Secure.php');
require_once('Validator.php');
require_once('Output.php');
require_once('Mail.php');
require_once('Sessions.php');
require_once('models/User.php');
require_once('UserController.php');

if(isset($_POST['register']))
{
    $email = Secure::getPostValue($_POST['email']);
    $phonenumber = Secure::getPostValue($_POST['phonenumber']);
    $password = Secure::getPostValue($_POST['password']);
    $rePassword = Secure::getPostValue($_POST['rePassword']);

    if (
        Validator::rePassword($password, $rePassword) &&
        Validator::validateInput("email", $email) &&
        Validator::validateInput("phone", $phonenumber) && 
        Validator::validateInput("password", $password) 
    ) {
        //okay
        if(!User::exists($email, $phonenumber)){
            if(UserController::registrate($email, $phonenumber, $password)){
                if (Mail::sendActivationEmail($email)) {
                        //successfully registered && activation code is sended
                        echo Output::UserSuccessfullyRegistered();
                }else{
                    //user activation code error
                    echo Output::UserSendActivationCodeError();
                }
            }else{
                //user registration error
                echo Output::UserRegistrationError();
            }
        }else{
            //user exists error
            echo Output::UserExistsError();
        }
    }else{
        //validation error
        echo Output::ValidationError();
    }

}

 if(isset($_POST['login']))
 {
    $email = Secure::getPostValue($_POST['email']);
    $password = Secure::getPostValue($_POST['password']);

    if(!Secure::BruteForce(User::getUserId($email))){
            if (
            Validator::validateInput("email", $email) &&
            Validator::validateInput("password", $password) 
        ) {
            //okay
            if(User::CanLogin($email, $password)){
                if(User::setSessionInfo($email)){
                    //successfully logined 
                    echo Output::UserSuccessfullyLogined();
                }else{
                    //session info setting error
                    echo Output::UserSessionInfoError();
                }
            }else{
                //user exists error
                echo Output::WrongCredentials();
            }
        }else{
            //validation error
            echo Output::ValidationError();
        }
    }else{
        //brute force
        echo Output::BruteForce();
    }
 }


 if(isset($_POST['deleteSession'])){
    if(Session::deleteSession(Secure::getPostValue($_POST['session']))){
        Output::SessionDeleted();
    }else{
         Output::SessionNotDeleted();
    }
 }


