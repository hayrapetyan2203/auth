<?php 

/**
 * 
 */
class Output
{
	public static function ValidationError()
	{
		$response = [
			'code' => 400,
			'message' => 'Validation Error.',
			'status' => false
		];
		return json_encode($response);
	}
		
	public static function activationCodeSetError()
	{
		$response = [
			'code' => 500,
			'message' => 'Activation Code was not setted.',
			'status' => false
		];
		return json_encode($response);
	}	

	public static function BruteForce()
	{
		$response = [
			'code' => 500,
			'message' => 'You are blocked for ... seconds!.',
			'status' => false
		];
		return json_encode($response);
	}	

	public static function SessionNotDeleted()
	{
		$response = [
			'code' => 500,
			'message' => 'Session was not deleted.',
			'status' => false
		];
		return json_encode($response);
	}

	public static function UserExistsError()
	{
		$response = [
			'code' => 400,
			'message' => 'User with that email or password already exists.',
			'status' => false
		];
		return json_encode($response);
	}	

	public static function WrongCredentials()
	{
		$response = [
			'code' => 400,
			'message' => 'Wrong email or password.',
			'status' => false
		];
		return json_encode($response);
	}	

	public static function UserSessionInfoError()
	{
		$response = [
			'code' => 400,
			'message' => 'User info was not sended.',
			'status' => false
		];
		return json_encode($response);
	}	

	public static function UserRegistrationError()
	{
		$response = [
			'code' => 400,
			'message' => 'Something went wrong with the registration process.',
			'status' => false
		];
		return json_encode($response);
	}

	public static function UserSuccessfullyRegistered()
	{
		$response = [
			'code' => 200,
			'message' => 'Successfully registered.',
			'status' => true
		];
		return json_encode($response);
	}	

	public static function SessionDeleted()
	{
		$response = [
			'code' => 200,
			'message' => 'Session Successfully  deleted.',
			'status' => true
		];
		return json_encode($response);
	}	

	public static function UserSuccessfullyLogined()
	{
		$response = [
			'code' => 200,
			'message' => 'Successfully logined.',
			'status' => true
		];
		return json_encode($response);
	}

	public static function UserSendActivationCodeError()
	{
		$response = [
			'code' => 500,
			'message' => 'There are an error with activation code sending process.',
			'status' => false
		];
		return json_encode($response);
	}
}
 ?>