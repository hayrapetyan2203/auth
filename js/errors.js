function showErrors(email, phonenumber, password){

	if (!validate('email', email)) {
		setErrorColors('error', 'email')
	}

	if (!validate('phonenumber', phonenumber)) {
		setErrorColors('error', 'phonenumber')
	}

	if (!validate('password', password)) {
		setErrorColors('error', 'password')
	}

	if (typeof rePassword !== 'undefined' && password !== rePassword) {
	   setErrorColors('error', 'rePassword')
	}
}

function setErrorColors(type, data){
  if(type == 'error'){
  	$('#' + data + 'Error').removeClass('text-muted')
  	$('#' + data + 'Error').addClass('text-danger')
  }

  if(type == 'success'){
  	$('#' + data + 'Error').removeClass('text-danger')
  	$('#' + data + 'Error').addClass('text-success')
  }
}

