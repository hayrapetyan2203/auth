<?php 

/**
 * 
 */
class UserController
{
	
	public static function registrate($email, $phonenumber, $password) {
   	  $password = password_hash($password, PASSWORD_DEFAULT);
	  $dbConn = Connect::getConnection();
	  $InsertUserQuery = $dbConn->prepare("INSERT INTO users (email, phonenumber, password) VALUES (:email, :phonenumber, :password)");
	  $InsertUserQuery->bindParam(':email', $email);
	  $InsertUserQuery->bindParam(':phonenumber', $phonenumber);
	  $InsertUserQuery->bindParam(':password', $password);
	  $success = $InsertUserQuery->execute();
	  return $success;
	}

}
 ?>