<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Auth system</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
  	<link rel="stylesheet" href="css/index.css">	
</head>
<body>
	<?php
		if(!isset($_GET['action'])){$_GET['action'] = 'register';}
		require_once($_GET['action'].'.php');
	?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script src="js/validator.js"></script>
<script src="js/errors.js"></script>
<script src="js/login.js"></script>
<script src="js/register.js"></script>
<script src="js/index.js"></script>
</body>
</html>