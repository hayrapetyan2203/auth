<?php 
require_once('models/Connect.php');

/**
 * 
 */
class Session
{
	
	public static function getUserSessionsCount($user_id)
	{
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("SELECT count(*) as count FROM usersession WHERE user_id = :user_id ");
	    $UserExistsQuery->bindParam(':user_id', $user_id);
	    $UserExistsQuery->execute();
	    $user = $UserExistsQuery->fetch(PDO::FETCH_ASSOC);
	    return ($user['count']);
	}	

	public static function getUserSessions($user_id)
	{
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("SELECT *  FROM usersession WHERE user_id = :user_id ");
	    $UserExistsQuery->bindParam(':user_id', $user_id);
	    $UserExistsQuery->execute();
	    $user = $UserExistsQuery->fetchAll(PDO::FETCH_ASSOC);
	    return $user;
	}	

	public static function deleteSession($session_id)
	{
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("DELETE FROM usersession WHERE session_id = :session_id ");
	    $UserExistsQuery->bindParam(':session_id', $session_id);
	    return $UserExistsQuery->execute();
	}

	public static function Check()
	{
		$session_id = $_COOKIE['PHPSESSID'];
		$dbConn = Connect::getConnection();
	    $UserExistsQuery = $dbConn->prepare("SELECT count(*) as count FROM usersession WHERE session_id = :session_id ");
	    $UserExistsQuery->bindParam(':session_id', $session_id);
	    $UserExistsQuery->execute();
	    $user = $UserExistsQuery->fetch(PDO::FETCH_ASSOC);
	    if(intval($user['count']) < 1){
	    	header('Location: ' . './index.php?action=login');
	    }
	}
}
 ?>